<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    //
    public function route1(){
    	return "<h3>Route-1 berhasil diakses</h3>";
    }
    public function route2(){
    	return "<h3>Route-2 berhasil diakses</h3>";
    }
    public function route3(){
    	return "<h3>Route-3 berhasil diakses</h3>";
    }
}
