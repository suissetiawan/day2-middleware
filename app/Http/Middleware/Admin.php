<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->isRole() == "sadmin") {
            $decision = $request->route()->named('route1')||$request->route()->named('route2')||$request->route()->named('route3');
        }else if(Auth::user()->isRole() == "admin") {
            $decision = $request->route()->named('route2')||$request->route()->named('route3');
        }else{
            $decision = $request->route()->named('route3');
        }
        
        if ($decision) {
            return $next($request);
        }else{
        abort(403);
        }
        
        
    }
}
