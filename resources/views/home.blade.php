@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p class="mb-5">Now,you are logged in as <b>{{Auth::user()->name}}</b>!!</p>
                    <a href="/route-1" class=" btn btn-primary mr-1">link route-1</a>
                    <a href="/route-2" class=" btn btn-primary mr-1">link route-2</a>
                    <a href="/route-3" class=" btn btn-primary">link route-3</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
